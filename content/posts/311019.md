Title: Work in progress
Date: 2019-10-31 21:54
Modified: 2019-10-31 21:54
Category: Blog
Tags: blog,progress
Slug: work-in-progress
Authors: jkw
Summary: Work in progress

First started the page at 21st. Or even a day earlier as I had to figure out how
the static page generator worked on Gitlab compared to Github since I switched over.

I think I've got a good enough setup now to generate the page. Well, it looks awful
but hey it's a site?

Will do my best to try to figure out how to make my own theme for Pelican to replace
it as soon as possible.

Well, I know how to do it, but I just need to make time to do the structural work
to work on the site theme at the same time as posting content under the same directory.

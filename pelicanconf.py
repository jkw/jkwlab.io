#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'jkw'
SITENAME = 'jkw of the internet'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Stockholm'

DEFAULT_LANG = 'en'
DISPLAY_CATEGORIES_ON_MENU = False
GOOGLE_ANALYTICS = 'UA-145147067-1'
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://python.org/'),
         ('GitLab', 'https://gitlab.com/'),)

# Social widget
SOCIAL = (('GitLab', 'https://gitlab.com/jkw'),
          ('Github', 'https://github.com/jkw'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# own

ARTICLE_URL = 'a/{date:%y}/{date:%m%d}-{slug}.html' # The URL to refer to an article.
ARTICLE_SAVE_AS = 'a/{date:%y}/{date:%m%d}-{slug}.html' # The place where we will save an article.
ARTICLE_LANG_URL = 'a/{date:%y}/{date:%m%d}-{slug}-{lang}.html' # The URL to refer to an article which doesn’t use the default language.
ARTICLE_LANG_SAVE_AS = 'a/{date:%y}/{date:%m%d}-{slug}-{lang}.html' # The place where we will save an article which doesn’t use the default language.
DRAFT_URL = 'd/{slug}.html' # The URL to refer to an article draft.
DRAFT_SAVE_AS = 'd/{slug}.html' # The place where we will save an article draft.
DRAFT_LANG_URL = 'd/{slug}-{lang}.html' # The URL to refer to an article draft which doesn’t use the default language.
DRAFT_LANG_SAVE_AS = 'd/{slug}-{lang}.html' # The place where we will save an article draft which doesn’t use the default language.
PAGE_URL = 'p/{slug}.html' # The URL we will use to link to a page.
PAGE_SAVE_AS = 'p/{slug}.html' # The location we will save the page. This value has to be the same as PAGE_URL or you need to use a rewrite in your server config.
PAGE_LANG_URL = 'p/{slug}-{lang}.html' # The URL we will use to link to a page which doesn’t use the default language.
PAGE_LANG_SAVE_AS = 'p/{slug}-{lang}.html' # The location we will save the page which doesn’t use the default language.
DRAFT_PAGE_URL = 'd/p/{slug}.html' # The URL used to link to a page draft.
DRAFT_PAGE_SAVE_AS = 'd/p/{slug}.html' # The actual location a page draft is saved at.
DRAFT_PAGE_LANG_URL = 'd/p/{slug}-{lang}.html' # The URL used to link to a page draft which doesn’t use the default language.
DRAFT_PAGE_LANG_SAVE_AS = 'd/p/{slug}-{lang}.html' # The actual location a page draft which doesn’t use the default language is saved at.
AUTHOR_URL = 'u/{slug}.html' # The URL to use for an author.
AUTHOR_SAVE_AS = 'u/{slug}.html' # The location to save an author.
CATEGORY_URL = 'c/{slug}.html' # The URL to use for a category.
CATEGORY_SAVE_AS = 'c/{slug}.html' # The location to save a category.
TAG_URL = 't/{slug}.html' # The URL to use for a tag.
TAG_SAVE_AS = 't/{slug}.html' # The location to save the tag page.
